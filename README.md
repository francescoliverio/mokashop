# Mokashop website - Based on Shopify

If you wanna edit this website and update the code directly inside the Shopify shop just follow these step:

1. git clone https://francescoliverio@bitbucket.org/francescoliverio/shopify.git
1. remove the directory Shopify_Formatter
1. Open a termina and type: "theme watch" in the root directory you have already download it. In this way when you edit a file automatically is connected to the Shopify Shop

# Ander Group - Shopify formatter

This type of script is very useful to be able to convert the file that is usually shot to the customer to compile to insert the products, in a format fully compatible with Shopify.

In order to get started, the following topics must be passed first:

1. Argument [1] = CSV Client from Google Drive
1. Argument [2] = Shopify CSV output
1. Argument [3] = Shopify header template

Download the all directory and then run this command:

```bash
python3 AnderGroup_formatter.py MOKA_ProductList.csv MOKA_output.csv shopify_product_template.csv
```

# Images import problem

If you find yourself having problems with the import of the products and as errors returns that did not find the image, to be able to import again, you must proceed in this way:

1. From the email that arrives at fan@ander.group you have to take only the file names of the images

1. Create a TXT file

1. Once you have created the TXT file, you have to insert it inside the folder that contains the images

1. Then create a folder with the images you need to upload again

1. Enter the folder that contains all the images and start the following script in bash from terminal:

```bash
for file in $(cat ./imagesToUpload.txt); do cp "$file" ../images_to_be_uploaded/; done
```

# TODO:

Add validator for each field:

1. Check the maximum characters
1. Check if the required field are compiled
1. Add quantity 1 when product doesn't has the quantity value - DONE
