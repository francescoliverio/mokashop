# Ander Group Shopify formatter
# Author Francesco Oliverio


import csv
import sys

# Argument [1] = Client CSV
# Argument [2] = Shopify CSV output
# Argument [3] = Shopify header template


def main():
    print("*** ANDER GROUP SHOPIFY FORMATTER ***")
    print("[INFO] moving items from {} to {}".format(
        sys.argv[1], sys.argv[2]))

    # Header of Shopify
    with open(sys.argv[3], "r") as sh:
        shopify_reader = csv.reader(sh)
        header_shopify = next(shopify_reader)

        # Initialize an empty Array
        shopify_products = []

    # Write the CSV output
    with open(sys.argv[2], "w", encoding='utf-8', errors='ignore') as o:
        writer = csv.writer(
            o, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(header_shopify)
        csv.field_size_limit(sys.maxsize)

        # Read the Client CSV
        with open(sys.argv[1], "r") as c:
            client_csv = csv.reader(c)
            next(client_csv)
            next(client_csv)

            # Initialize the counter
            product_handle_phase_1 = 100000

            # Mapping of all columns
            for row in client_csv:

                # Handle
                product_handle_phase_1 = product_handle_phase_1 + 1
                product_id = row[0]
                ean = row[1]
                vendor = marca = row[2]

                print(vendor)

                # SKU is generated based on first two letter of the vendor + product_handle
                sku = vendor[0:2].upper() + str(product_handle_phase_1)
                product_handle = sku

            # Product Title
                product_title_italian = row[3]
                product_title_english = row[4]
                product_title_german = row[5]
                product_title_french = row[6]

            # Product Description
                product_description_italian = row[7]
                product_description_english = row[8]
                product_description_german = row[9]
                product_description_french = row[10]

            # Price
                product_price = row[11]
                product_price_discounted = row[12]

            # Inventory
                product_cost = row[13]
                product_quantity_stock = row[14]

            # Weight
                product_weight = row[15]

            # Here you need to find the CDN url of the products
                shopify_cdn = "https://cdn.shopify.com/s/files/1/0373/8065/3187/files/"

            # Images
                product_image_1 = shopify_cdn + row[16]
                product_image_2 = row[17]
                product_image_3 = row[18]
                product_image_4 = row[19]

            # Video Link
                product_video_link_youtube = row[20]

            # Categories

                # Promozioni
                product_category_1 = row[21]

                # Novità
                product_category_2 = row[22]

                # Caffè in capsule
                product_category_3 = row[23]

                # Caffè in grani
                product_category_4 = row[24]

                # Caffè in cialde
                product_category_5 = row[25]

                # Caffè macinato
                product_category_6 = row[26]

                # Caffè Solubile
                product_category_7 = row[27]

                # Specialità al caffè
                product_category_8 = row[28]

                # Tè e tisane
                product_category_9 = row[29]

                # Macchine a capsule
                product_category_10 = row[30]

                # Macchine a cialde
                product_category_11 = row[31]

                # Macchine automatiche
                product_category_12 = row[32]

                # Macchine manuali
                product_category_13 = row[33]

                # Macchine professionali
                product_category_14 = row[34]

                # Accessori macchine caffé
                product_category_15 = row[35]

                # Elettrodomestici
                product_category_16 = row[36]

                # Profumatori per ambiente
                product_category_17 = row[37]

                # Bicchieri, posate e accessori
                product_category_18 = row[38]

                # Gastronomia italiana
                product_category_19 = row[39]

                # Cioccolata e dessert
                product_category_20 = row[40]

                # Succhi
                product_category_21 = row[41]

                # Caramelle e dolciumi
                product_category_22 = row[42]

                # Append the products in the right position

                # Title
                title = product_title_italian

                # Body (HTML)
                body = product_description_italian

                # Vendor
                vendor = vendor

                # Type
                type = ""

                # Tags
                tags = []

                # Append vendor tags for query in collections:
                # Example: https://newmokashop.myshopify.com/collections/caffe-solubile/borbone
                tags.append(vendor)

                if product_category_1 == "1":
                    category_1 = "Promozioni"
                    tags.append(category_1)

                if product_category_2 == "1":
                    category_2 = "Novità"
                    tags.append(category_2)

                if product_category_3 == "1":
                    category_3 = "Caffè in capsule"
                    tags.append(category_3)

                if product_category_4 == "1":
                    category_4 = "Caffè in grani"
                    tags.append(category_4)

                if product_category_5 == "1":
                    category_5 = "Caffè in cialde"
                    tags.append(category_5)

                if product_category_6 == "1":
                    category_6 = "Caffè macinato"
                    tags.append(category_6)

                if product_category_7 == "1":
                    category_7 = "Caffè Solubile"
                    tags.append(category_7)

                if product_category_8 == "1":
                    category_8 = "Specialità al caffè"
                    tags.append(category_8)

                if product_category_9 == "1":
                    category_9 = "Tè e tisane"
                    tags.append(category_9)

                if product_category_10 == "1":
                    category_10 = "Macchine a capsule"
                    tags.append(category_10)

                if product_category_11 == "1":
                    category_11 = "Macchine a cialde"
                    tags.append(category_11)

                if product_category_12 == "1":
                    category_12 = "Macchine automatiche"
                    tags.append(category_12)

                if product_category_13 == "1":
                    category_13 = "Macchine manuali"
                    tags.append(category_13)

                if product_category_14 == "1":
                    category_14 = "Macchine professionali"
                    tags.append(category_14)

                if product_category_15 == "1":
                    category_15 = "Accessori macchine caffé"
                    tags.append(category_15)

                if product_category_16 == "1":
                    category_16 = "Elettrodomestici"
                    tags.append(category_16)

                if product_category_17 == "1":
                    category_17 = "Profumatori per ambiente"
                    tags.append(category_17)

                if product_category_18 == "1":
                    category_18 = "Bicchieri, posate e accessori"
                    tags.append(category_18)

                if product_category_19 == "1":
                    category_19 = "Gastronomia italiana"
                    tags.append(category_19)

                if product_category_20 == "1":
                    category_20 = "Cioccolata e dessert"
                    tags.append(category_20)

                if product_category_21 == "1":
                    category_21 = "Succhi"
                    tags.append(category_21)

                if product_category_22 == "1":
                    category_22 = "Caramelle e dolciumi"
                    tags.append(category_22)

                # Convert array tags into a list of tags:
                tags = ' , '.join(tags)

                # Published
                published = "TRUE"

                # Option1 Name
                option1_name = ""

                # Option1 Value
                option1_value = ""

                # Option2 Name
                option2_name = ""

                # Option2 Value
                option2_value = ""

                # Option3 Name
                option3_name = ""

                # Option3 Value
                option3_value = ""

                # Variant SKU
                variant_sku = sku

                # Variant Grams
                variant_grams = product_weight

                # Variant Inventory Tracker
                variant_inventory_tracker = "shopify"

                # Variant Inventory Qty
                variant_inventory_quantity = product_quantity_stock

                # Validator integer quantity
                if variant_inventory_quantity in (None, ""):
                    print("This product {} has has no quantity. The quantity value is empty. For a right import we set the quantity with 1".format(
                        product_title_italian))
                    variant_inventory_quantity = "1"

                # Variant Inventory Policy
                variant_inventory_policy = "deny"

                # Variant Fulfillment Service
                variant_fulfillment_service = "manual"

                # Variant Price
                variant_price = product_price

                # Variant Compare At Price
                variant_compare_at_price = ""

                # Variant Requires Shipping
                variant_requires_shipping = "TRUE"

                # Variant Taxable
                variant_taxable = "TRUE"

                # Variant Barcode
                variant_barcode = ean

                # Image Src
                image_src = product_image_1

                # Image Position
                image_position = "1"

                # Image Alt Text
                image_alt_text = title

                # Validate the characters limit:
                if len(image_alt_text) > 512:
                    print("This text:{} is bigger then 512 characters".format(
                        image_alt_text))

                # Gift Card
                gift_card = "FALSE"

                # SEO Title
                seo_title = title

                # SEO Description
                seo_description = body

                # Google Shopping / Google Product Category
                google_product_category = ""

                # Google Shopping / Gender
                google_gender = ""

                # Google Shopping / Age Group
                google_age_group = ""

                # Google Shopping / MPN
                google_mpn = ""

                # Google Shopping / AdWords Grouping
                google_adwords_grouping = ""

                # Google Shopping / AdWords Labels
                google_adwords_labels = ""

                # Google Shopping / Condition
                google_condition = ""

                # Google Shopping / Custom Product
                google_custom_product = ""

                # Google Shopping / Custom Label 0
                google_custom_label_0 = ""

                # Google Shopping / Custom Label 1
                google_custom_label_1 = ""

                # Google Shopping / Custom Label 2
                google_custom_label_2 = ""

                # Google Shopping / Custom Label 3
                google_custom_label_3 = ""

                # Google Shopping / Custom Label 4
                google_custom_label_4 = ""

                # Variant Image
                variant_image = ""

                # Variant Weight Unit
                variant_weight_unit = "g"

                # Variant Tax Code
                variant_tax_code = ""

                # Cost per item
                cost_per_item = ""

                # Append variable to the list
                shopify_products.append(
                    [product_handle,
                     title,
                     body,
                     vendor,
                     type,
                     tags,
                     published,
                     option1_name,
                     option1_value,
                     option2_name,
                     option2_value,
                     option3_name,
                     option3_value,
                     variant_sku,
                     variant_grams,
                     variant_inventory_tracker,
                     variant_inventory_quantity,
                     variant_inventory_policy,
                     variant_fulfillment_service,
                     variant_price,
                     variant_compare_at_price,
                     variant_requires_shipping,
                     variant_taxable,
                     variant_barcode,
                     image_src,
                     image_position,
                     image_alt_text,
                     gift_card,
                     seo_title,
                     seo_description,
                     google_product_category,
                     google_gender,
                     google_age_group,
                     google_mpn,
                     google_adwords_grouping,
                     google_adwords_labels,
                     google_condition,
                     google_custom_product,
                     google_custom_label_0,
                     google_custom_label_1,
                     google_custom_label_2,
                     google_custom_label_3,
                     google_custom_label_4,
                     variant_image,
                     variant_weight_unit,
                     variant_tax_code,
                     cost_per_item])

                # Check if there are other images

                if product_image_2 in (None, ""):
                    pass
                else:
                    product_image_2 = shopify_cdn + product_image_2

                    # Append new Row just with the image link
                    shopify_products.append([
                        product_handle,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        product_image_2,
                        "",
                        seo_title,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                    ])

                if product_image_3 in (None, ""):
                    pass
                else:
                    product_image_3 = shopify_cdn + product_image_3
                    # Append new Row just with the image link
                    shopify_products.append([
                        product_handle,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        product_image_3,
                        "",
                        seo_title,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                    ])

                if product_image_4 in (None, ""):
                    pass
                else:
                    product_image_4 = shopify_cdn + product_image_4
                    # Append new Row just with the image link
                    shopify_products.append([
                        product_handle,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        product_image_4,
                        "",
                        seo_title,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                    ])

        # Write the Array inside the new file
        writer.writerows(shopify_products)


if __name__ == "__main__":
    main()
